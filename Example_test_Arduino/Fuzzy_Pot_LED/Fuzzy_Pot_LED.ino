// Fuzzy control
/*
 * Use Potentiometer as input
 * Use LED as output
 * Use Fuzzy Logic to experiment
 * Last update : 2017/09/20
 */
float Light_NM[11] = {1,0.9,0.8,0.7,0.6,0,0,0,0,0,0};
float Light_Z[11] = {0,0,0,0,0,1,0,0,0,0,0};
float Light_PM[11] = {0,0,0,0,0,0,0.6,0.7,0.8,0.9,1};

float Light_output;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(A0,INPUT);
  pinMode(8,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  int p;
  p = analogRead(A0);
  p = map(p,0,1023,-45,45);
  Light_Out(p);
  int out;
  out = map(Light_output,-3.25,3.25,0,255);
  analogWrite(8,out);
  Serial.print("Input: "); Serial.print(p); Serial.print(" , Output: "); Serial.println(Light_output);
  delay(1000);
}


void Light_Out(int data){
  int i;
  float map_i, v_i, area, moment;
  float u_l;
  float u_Gas[11]={};

  // Rule 1
  u_l = 0;
  if((data >= 5) && (data<=45)){
   u_l = (float)(data-5)/40;    // 歸屬度計算
  }
  Serial.print("Rule 1: "); Serial.println(u_l);
  if(u_l != 0){
    for(i=0;i<11;i++){
      u_Gas[i] = max(u_Gas[i], min(u_l, Light_PM[i]));
      //Serial.println(u_Gas[i]);
    }
  }
  
  // Rule 2   在 Z 位置
  u_l = 0;
  if((data >= -10) && (data <= 0)){
   u_l = (float)(data+10)/10;       // 歸屬度計算
  }
  if((data > 0) && (data < 10)){
   u_l = (float)(10-data)/10;      // 歸屬度計算
  }
  Serial.print("Rule 2: "); Serial.println(u_l);
  if(u_l != 0){
    for(i=0;i<11;i++){
      u_Gas[i] = max(u_Gas[i], min(u_l, Light_Z[i]));
      //Serial.println(u_Gas[i]);
    }
  }
  
  // Rule 3
  u_l = 0;
  if((data >=-45) && (data<=-5)){
   u_l = (float)(-5-data)/40;       // 歸屬度計算
  }
  Serial.print("Rule 3: "); Serial.println(u_l);
  if(u_l != 0){
    for(i=0;i<11;i++){
      u_Gas[i] = max(u_Gas[i], min(u_l, Light_NM[i]));
      //Serial.println(u_Gas[i]);
    }
  }

  // 解模糊化 Use 重心法
  area = 0.0;
  moment = 0.0;
  for(i=0;i<11;i++){
    map_i = u_Gas[i];
    Serial.print("map: "); Serial.println(map_i);
    v_i = (-5+i);
    area += map_i;
    moment += map_i * v_i;
  }
  Serial.print("area: "); Serial.print(area); Serial.print(" , moment: "); Serial.println(moment);
  if(area == 0){     // 都沒有在 Rule 規則裡面
    Light_output = 0;
  }
  else{
    Light_output = moment/area;
  }
}







