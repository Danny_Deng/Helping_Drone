#include <TinyGPS.h>
TinyGPS gps;
boolean gps_flag = false;
float latitude, longitude;   // 緯度, 經度
unsigned long age;    // Fix  Date


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial3.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial3.available()){
     while (Serial3.available()){
      char c = Serial3.read();
      // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c)) // Did a new valid sentence come in?
        gps_flag = true;
    }
  }
  if(gps_flag){
    gps.f_get_position(&latitude, &longitude, &age);
    latitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : latitude, 6;
    longitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : longitude, 6;
    Serial.print("Position: ");
    Serial.print("latitude: ");Serial.print(latitude);Serial.print(" ");// print latitude
    Serial.print("longitude: ");Serial.println(longitude); // print longitude
    gps_flag = false;
  }


}
