
void Ultra_setup() {
  // put your setup code here, to run once:
  pinMode(Ultra_front_trig, OUTPUT);
  pinMode(Ultra_front_echo, INPUT);
  pinMode(Ultra_back_trig, OUTPUT);
  pinMode(Ultra_back_echo, INPUT);
  pinMode(Ultra_left_trig, OUTPUT);
  pinMode(Ultra_left_echo, INPUT);
  pinMode(Ultra_right_trig, OUTPUT);
  pinMode(Ultra_right_echo, INPUT);
  pinMode(Ultra_bottom_trig, OUTPUT);
  pinMode(Ultra_bottom_echo, INPUT);
}

/*
 * 假設目前平均溫度 30度
 * 超音波速度: 340 + 0.6*30 = 349 m/s = 349*100/1000000 = 0.0349  cm/us
 * us per cm = 1/0.0349 = 28.65 us/cm
 * 設定最大量測距離 150 cm: 來回秒數為: 28.65*150*2 = 8595.99   --> 進位:8600
 * 距離換算: 時間乘上速度/2(來回) = duration*0.0349/2 = duration*0.01745
 */
void Get_Ultra_Front(){
  int duration;
  digitalWrite(Ultra_front_trig, HIGH);
  delayMicroseconds(1000);
  digitalWrite(Ultra_front_trig, LOW); 
  duration = pulseIn(Ultra_front_echo, HIGH, 8600); 
  distance_front = duration*0.01745;
  if(distance_front <= 0){
    distance_front = 150;
  }
}
//****************************************************
void Get_Ultra_Back(){
  int duration;
  digitalWrite(Ultra_back_trig, HIGH);
  delayMicroseconds(1000);
  digitalWrite(Ultra_back_trig, LOW); 
  duration = pulseIn(Ultra_back_echo, HIGH, 8600); 
  distance_back = duration*0.01745;
  if(distance_back <= 0){
    distance_back = 150;
  }
}
//****************************************************
void Get_Ultra_Left(){
  int duration;
  digitalWrite(Ultra_left_trig, HIGH);
  delayMicroseconds(1000);
  digitalWrite(Ultra_left_trig, LOW); 
  duration = pulseIn(Ultra_left_echo, HIGH, 8600); 
  distance_left = duration*0.01745;
  if(distance_left <= 0){
    distance_left = 150;
  }
}
//****************************************************
void Get_Ultra_Right(){
  int duration;
  digitalWrite(Ultra_right_trig, HIGH);
  delayMicroseconds(1000);
  digitalWrite(Ultra_right_trig, LOW); 
  duration = pulseIn(Ultra_right_echo, HIGH, 8600); 
  distance_right = duration*0.01745;
  if(distance_right <= 0){
    distance_right = 150;
  }
}
//****************************************************
void Get_Ultra_Bottom(){
  int duration;
  digitalWrite(Ultra_bottom_trig, HIGH);
  delayMicroseconds(1000);
  digitalWrite(Ultra_bottom_trig, LOW); 
  duration = pulseIn(Ultra_bottom_echo, HIGH, 8600); 
  distance_bottom = duration*0.01745;
  if(distance_bottom <= 0){
    distance_bottom = 150;
  }
}
//****************************************************
