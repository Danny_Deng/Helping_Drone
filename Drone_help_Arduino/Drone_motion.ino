/*
 * Drone Motion Code write here
 * Motor order (順序)
 *     motor_w2    motor_r1
 *     motor_w1    motor_r2
 * Reference Website:http://kitsprout.logdown.com/posts/335383
 * 白1 : 66~160  CCW
 * 白2 : 65~165  CW
 * 紅1 : 52~160  CCW
 * 紅2 : 65~165  CW
美國手~~~左手 前後 --> 前進後退
         左手 左右 --> 順逆旋轉
         右手 前後 --> 上下飛行
         右手 左右 --> 左右飛行
 * 實際測試: unit : us
 * Ch1:1035~1901   左~右  middle:1470
 * Ch2:1035~1892   上~下  middle:1470
 * Ch3:1035~1901   上~下  middle:1470
 * Ch4:1035~1895   左~右  middle:1470
 * Ch5: C，三段式 1066, 1489, 1897 --> 臨界值:1278, 1693
 */

void Motor_setup(){
  motor_w1.attach(motor_w1_pin);
  motor_w2.attach(motor_w2_pin);
  motor_r1.attach(motor_r1_pin);
  motor_r2.attach(motor_r2_pin);
  motor_w1.write(0);
  motor_w2.write(0);
  motor_r1.write(0);
  motor_r2.write(0);
}

void Motor_update(){
    motor_w1.write(info_w1);
    motor_w2.write(info_w2);
    motor_r1.write(info_r1);
    motor_r2.write(info_r2);
}

void Motor_init(){
  int i;
  for(i=0;i<80;i++){
    info_w1 = i;
    info_w2 = i;
    info_r1 = i;
    info_r2 = i;
    Motor_update();
    delay(100);
  }
}

void Motion_test(){
  if(Activate_flag == true){
    info_w1 = map(signal1, 1000, 1901, 0, 165);
    info_w2 = map(signal2, 1000, 1901, 0, 165);
    info_r1 = map(signal3, 1000, 1901, 0, 165);
    info_r2 = map(signal4, 1000, 1901, 0, 165);
    Motor_update();
  }
  else{
    motor_w1.write(0);
    motor_w2.write(0);
    motor_r1.write(0);
    motor_r2.write(0);
  }
}


void Motion_update(){
  float angle_w1, angle_w2, angle_r1, angle_r2;
  float increase, increase_w1, increase_w2, increase_r1, increase_r2;
  
// 機體向左飛行 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-遙控器右手 搖桿左右，左:w1,r2 快，右:w2,r1 快  --> ch1
  if(signal1 < 1400){
    increase = map(signal1, 1000, 1400, 45, 0); 
    increase_w1 += increase;
    increase_r2 += increase;
  }
  else if(signal1 > 1600){
    increase = map(signal1, 1600, 1920, 0, 45); 
    increase_w2 += increase;
    increase_r1 += increase;
  }

// 機體上下飛行 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-遙控器右手 搖桿前後，上升:全部 快，下降:全部 慢  --> ch2
  if(signal2 < 1400){
    increase = map(signal2, 1000, 1400, 45, 0); 
    increase_w1 += increase;
    increase_w2 += increase;
    increase_r1 += increase;
    increase_r2 += increase;
  }
  else if(signal2 > 1600){
    increase = map(signal2, 1600, 1920, 0, 45); 
    increase_w1 -= increase;
    increase_w2 -= increase;
    increase_r1 -= increase;
    increase_r2 -= increase;
  }

// 機體前後飛行 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-遙控器左手 搖桿前後，前:w1,w2 快，後:r1,r2 快  --> ch3
  if(signal3 < 1400){
    increase = map(signal3, 1000, 1400, 45, 0); 
    increase_w1 += increase;
    increase_w2 += increase;
  }
  else if(signal3 > 1600){
    increase = map(signal3, 1600, 1920, 0, 45); 
    increase_r1 += increase;
    increase_r2 += increase;
  }
  
 // 機體旋轉 -*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-遙控器左手 搖桿左右，順時針:w1,r1 快，逆時針:w2,r2 快  --> ch4
  if(signal4 < 1400){
    increase = map(signal4, 1000, 1400, 0, 45); 
    increase_w2 += increase;
    increase_r2 += increase;
  }
  else if(signal4 > 1600){
    increase = map(signal4, 1600, 1920, 0, 45); 
    increase_w1 += increase;
    increase_r1 += increase;
  }

// Fuzzy correct************************************************

    increase_w2 += roll_corr;
    increase_r1 += roll_corr;
    increase_w1 += -roll_corr;   // 負負得正
    increase_r2 += -roll_corr;   // 負負得正



    increase_w1 += pitch_corr;
    increase_w2 += pitch_corr;
    increase_r1 += -pitch_corr;   // 負負得正
    increase_r2 += -pitch_corr;   // 負負得正


    increase_w1 += height_corr;
    increase_w2 += height_corr;
    increase_r1 += height_corr;
    increase_r2 += height_corr;

    increase_w1 += -height_corr;   // 負負得正
    increase_w2 += -height_corr;   // 負負得正
    increase_r1 += -height_corr;   // 負負得正
    increase_r2 += -height_corr;   // 負負得正


// Output************************************************
  info_w1 = base_w1 + increase_w1;
  info_w2 = base_w2 + increase_w2;
  info_r1 = base_r1 + increase_r1;
  info_r2 = base_r2 + increase_r2;
  
  if(info_w1 <= base_w1){info_w1 = base_w1;}
  else if(info_w1>=120){info_w1 = 140;}
  if(info_w2 <= base_w2){info_w2 = base_w2;}
  else if(info_w2>=120){info_w2 = 140;}
  if(info_r1 <= base_r1){info_r1 = base_r1;}
  else if(info_r1>=120){info_r1 = 140;}
  if(info_r2 <= base_r2){info_r2 = base_r2;}
  else if(info_r2>=120){info_r2 = 140;}
  
  Motor_update();
}


